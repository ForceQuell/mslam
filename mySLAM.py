# mySLAM based on opticalFlow Lucas-Kanade

import numpy as np
import cv2


def getAbsoluteScale(f, frame_id):
    x_pre, y_pre, z_pre = f[frame_id - 1][3], f[frame_id - 1][7], f[frame_id - 1][11]
    x, y, z = f[frame_id][3], f[frame_id][7], f[frame_id][11]
    scale = np.sqrt((x - x_pre) ** 2 + (y - y_pre) ** 2 + (z - z_pre) ** 2)
    return x, y, z, scale

def getTruePose(n):
    global mainDataSetPath
    return np.genfromtxt(mainDataSetPath + 'poses/%02d.txt' % n, delimiter=' ', dtype=None)

def getImage(imageNum):
    global mainDataSetPath
    global seqNum
    return cv2.imread(mainDataSetPath + 'sequences/%02d/image_2/%06d.png' % (seqNum, imageNum), 1)

def getCameraInternalParams(alphaX, alphaY, U, V, gamma=0):
    return np.array([[alphaX, gamma, U],
                     [0, alphaY, V],
                     [0, 0, 1]])

def featureDetection():
    thresh = dict(threshold=25, nonmaxSuppression=True)
    fast = cv2.FastFeatureDetector_create(**thresh)
    return fast

def featureTracking(preImg, curImg, preFeatures):
    lk_params = dict(winSize=(17, 17),
                     maxLevel=4,
                     criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 30, 0.01))

    curFeatures, st, err = cv2.calcOpticalFlowPyrLK(preImg, curImg, preFeatures, None, **lk_params)

    st = st.reshape(st.shape[0])

    ##find good one
    preFeatures = preFeatures[st == 1]
    curFeatures = curFeatures[st == 1]
    return preFeatures, curFeatures

def description(image, features):
    kps = [cv2.KeyPoint(x=f[0], y=f[1], _size=20) for f in features]
    kps, des = cv2.ORB_create().compute(image, kps)
    return kps, des

def matching(preDes, curDes, preFeatures, curFeatures):
    global bf
    matches = bf.knnMatch(preDes, curDes, k=2)

    matchPoints = []

    for m, n in matches:
        if m.distance < 0.75 * n.distance:
            matchPoints.append((preFeatures[m.queryIdx], curFeatures[m.trainIdx]))

    return matchPoints

def drawFeatures_for_opticalFlow(image, curFeatures):
    for f in curFeatures:
        cv2.circle(image, (int(f[0]), int(f[1])), radius=2, color=featureColor, thickness=1)

def drawFeatures(image, curfeatures, matches=None):
    if matches is not None:
        for m in matches:
            p = int(m)
            cv2.circle(image, p, radius=2, color=featureColor)
    else:
        for f in curfeatures:
            cv2.circle(image, (int(f.pt[0]), int(f.pt[1])), radius=2, color=255)
        # cv2.circle(image, (int(f.pt[0]), int(f.pt[1])), radius=2, color=255)

def drawTracks(image, matches, preFeatures, curFeatures):
    thres = 63
    for match in matches:
        if match[0].distance > thres:
            break
        queryIdx = match[0].queryIdx
        trainIdx = match[0].trainIdx
        cv2.line(image, (int(preFeatures[queryIdx].pt[0]), int(preFeatures[queryIdx].pt[1])),
                 (int(curFeatures[trainIdx].pt[0]), int(curFeatures[trainIdx].pt[1])), color=trackColor)
        # cv2.line(image, tuple(*preF), tuple(*curF), color=(255))

def drawTracks_(image, matches, preFeatures, curFeatures):
    thres = 63
    for match in matches:
        if (match[0].distance > thres):
            break
        queryIdx = match[0].queryIdx
        trainIdx = match[0].trainIdx
        cv2.line(image, (int(preFeatures[queryIdx, 0]), int(preFeatures[queryIdx, 1])),
                 (int(curFeatures[trainIdx, 0]), int(curFeatures[trainIdx, 1])), color=trackColor)

def drawTracks_forOpticalFlow(image, preFeatures, curFeatures):
    for pre, cur in zip(preFeatures, curFeatures):
        cv2.line(image, tuple(pre), tuple(cur), trackColor)

def myColorImgConcat(img1, img2):
    H = np.max([img1.shape[0], img2.shape[0]])
    W = img1.shape[1] + img2.shape[1]

    ret = np.zeros((H, W, 3), np.uint8)
    ret[0:img1.shape[0], 0:img1.shape[1], :] = img1
    ret[0:img2.shape[0], img1.shape[1]:img1.shape[1] + img2.shape[1], :] = img2
    return ret

def myColorMerge(parent, child):
    H, W = parent.shape[0], parent.shape[1]
    h, w = child.shape[0], child.shape[1]
    ret = np.zeros( (H, W, 3), np.uint8 )

    ret[0:H, 0:W, :] = parent
    ret[0:h, W-w:W, :] = child
    return ret

def getAngleFromR(m):
    traceR = ((m[0, 0] + m[1, 1] + m[2, 2]) - 1) * 0.5
    theta = np.arccos((traceR-1)*0.5)
    om = 1/(2*np.sin(theta))
    ret = np.array([ om * (m[1, 2] - m[2, 1]), om * (m[2, 0] - m[0, 2]), om * (m[0, 1] - m[1, 0]) ])
    return theta, ret

def vecNorm(v):
    l = np.sqrt(v[0]**2 + v[1]**2 + v[2]**2)
    ret = np.array( [v[0] / l, v[1] / l, v[2] / l] )
    return ret

def vec2dNorm(v):
    l = np.sqrt(v[0]**2 + v[1]**2)
    ret = np.array( [v[0] / l, v[1] / l] )
    return ret

def drawArrow(img, p1:np.array, dir:np.array, len:float, color:tuple):
    dir = vec2dNorm(dir)*len
    p2 = tuple((p1 + dir).astype(int))
    p1 = tuple(p1.astype(int))
    cv2.line(img, p1, p2, color)

def drawText(img, text:str, p:tuple):
    cv2.rectangle(img, (p[0]-2, p[1]-2), (p[0]+10, p[1]+80), (0, 0, 0), cv2.FILLED)
    cv2.putText(img, text, p, cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 255), 1, 8)

# initialization
seqNum = 0
featureColor = (255, 0, 0)
trackColor = (0, 255, 0)

frameInterval = 1
startFrame = 1

lastSeqNumber = 21
mainDataSetPath = "/home/forcequell/kitti/dataset/"
outputTxtPath = "resTxt/"
outputImgPath = "resImg/"
outputGraphPath = "resGraph/"
diagnosticPath = "diagnostic/"

trajWinW, trajWinH = 1280, 1280
imageWinW, imageWinH = 1241, 376
lastNumbersOfSeq = np.array([4540, 1100, 4660, 800, 270, 2760, 1100, 1100, 4070, 1590, 1200, 920, 1060, 3280, 630, 1900, 1730, 490, 1800, 4980, 830, 2720])

MIN_NUM_FEAT = 1500

fc = 718.8560
pp = (607.1928, 185.2157)

imageWin = np.zeros((imageWinW, imageWinH, 3), np.uint8)
trajWin = np.zeros((trajWinW, trajWinH, 3), np.uint8)

groundTruth = getTruePose(seqNum)
detector = featureDetection()

draw_x, draw_y, draw_tx, draw_ty = [], [], [], []

vecDir = np.array([0, 0, 50])
vecDir_tmp = np.array([0, 0, 50])

# pre-iteration
preImg = getImage(startFrame)

preFeatures = np.array([el.pt for el in detector.detect(preImg)], dtype='float32')

drawFeatures_for_opticalFlow(preImg, preFeatures)

R_ac = np.array([[1, 0, 0],
                 [0, 1, 0],
                 [0, 0, 1]])
t_ac = np.array([[0],
                 [0],
                 [0]])
distance = 0
truth_x, truth_y, truth_z = 0, 0, 0

for i in range(startFrame+1, lastNumbersOfSeq[seqNum] + 1):
    # main alg {
    if (len(preFeatures) < MIN_NUM_FEAT):
        preFeatures = np.array([ele.pt for ele in detector.detect(preImg)], dtype='float32')

    curImg = getImage(i)

    curFeatures = np.array([el.pt for el in detector.detect(curImg)], dtype='float32')

    preMatch, curMatch = featureTracking(preImg, curImg, preFeatures)

    E, mask = cv2.findEssentialMat(preMatch, curMatch, fc, pp, cv2.RANSAC, 0.999, 1.0)

    mask = mask.reshape(mask.shape[0])
    preMatch = preMatch[mask == 1]
    curMatch = curMatch[mask == 1]

    _, R, t, mask = cv2.recoverPose(E, preMatch, curMatch, focal=fc, pp=pp)

    preFeatures = curFeatures
    preImg = curImg
    # main alg }

    # {
    truth_x_new, truth_y_new, truth_z_new, absolute_scale = getAbsoluteScale(groundTruth, i)
    distance += np.sqrt((truth_x_new - truth_x) ** 2 + (truth_y_new - truth_y) ** 2 + (truth_z_new - truth_z) ** 2)
    truth_x, truth_y, truth_z = truth_x_new, truth_y_new, truth_z_new

    if absolute_scale > 0.1:
        t_ac = t_ac + absolute_scale * R_ac @ t  # R_ac.dot(t)
        R_ac = R @ R_ac
        vecDir = vecDir @ R

    R_gt = np.array([[groundTruth[i, 0], groundTruth[i, 1], groundTruth[i, 2]],
                     [groundTruth[i, 4], groundTruth[i, 5], groundTruth[i, 6]],
                     [groundTruth[i, 8], groundTruth[i, 9], groundTruth[i, 10]]])

    vecDir_truth = vecDir_tmp @ R_gt
    tmp = vecDir_truth.copy()
    tmp[0] *= -1

    curAngErr = np.rad2deg(np.arccos(( vecDir @ tmp.T ) /( np.linalg.norm(vecDir) * np.linalg.norm(tmp) )))
    curTrsErr = np.sqrt((truth_x - t_ac[0]) ** 2 + (truth_z + t_ac[2]) ** 2)
    # }

    # drawing {
    cv2.rectangle(trajWin, (0, 0), (trajWinW, trajWinH), (0, 0, 0), cv2.FILLED)
    drawFeatures_for_opticalFlow(curImg, curMatch)
    drawTracks_forOpticalFlow(curImg, preMatch, curMatch)
    mergedImg = myColorMerge(trajWin, curImg)

    draw_x.append(int(t_ac[0]) + trajWinW // 2)
    draw_y.append(-int(t_ac[2]) + trajWinH // 2)
    draw_tx.append(int(truth_x) + trajWinW // 2)
    draw_ty.append(int(truth_z) + trajWinH // 2)

    for px, py, ptx, pty in zip(draw_x, draw_y, draw_tx, draw_ty): # drawing trajectories
        cv2.circle(mergedImg, (px, py), 1, (0, 0, 255), 2)
        cv2.circle(mergedImg, (ptx, pty), 1, (0, 255, 0), 2)

    last_tx = draw_tx[draw_tx.__len__() - 1]
    last_ty = draw_ty[draw_ty.__len__() - 1]
    last_x = draw_x[draw_x.__len__() - 1]
    last_y = draw_y[draw_y.__len__() - 1]
    cv2.circle(mergedImg, (last_tx, last_ty), 7, (0, 255, 0), 3)
    cv2.circle(mergedImg, (last_x, last_y), 7, (0, 0, 255), 3)

    drawArrow(mergedImg, np.array([last_tx, last_ty]), np.array([-vecDir_truth[0], vecDir_truth[2]]), 50, (0,255,0))
    drawArrow(mergedImg, np.array([last_x, last_y]), np.array([vecDir[0], vecDir[2]]), 50, (0, 0, 255))

    drawText(mergedImg, "FRAME: %d" % i, (10, 400))
    drawText(mergedImg, "TRS ERR: %f [m]" % curTrsErr, (10, 420))
    drawText(mergedImg, "ANG ERR: %f [deg]" % curAngErr, (10, 440))

    cv2.imshow("seq%02d" % seqNum, mergedImg)
    # drawing }

    cv2.imwrite(diagnosticPath + ("seq%02d/" % seqNum) + ("frame%d.png" % i), mergedImg)

    k = cv2.waitKey(frameInterval) & 0xFF
    if k == 27:
        break
# ========================================================end cicle

cv2.destroyAllWindows()
